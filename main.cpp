#include<cstdio>
#include<string>
// #define WIN32_LEAN_AND_MEAN
// #include<Windows.h>
#include<iostream>
#include<fstream>
// #include"include/HTTPRequest.hpp"

//#define CPPHTTPLIB_OPENSSL_SUPPORT
//#include"include/httplib.h"

#include<Windows.h>
#include<WinInet.h>
#include<comdef.h>

//#pragma comment(lib, "urlmon.lib")

//#include <urlmon.h>
#include <sstream>

#pragma warning(disable : 4996)
std::string get_mod_name_from_xml(std::string path) {
	auto fdesc = fopen(path.c_str(),"r");
	if (fdesc) {
		fseek(fdesc, 0, SEEK_END);
		int fdesc_size = ftell(fdesc);
		rewind(fdesc);
		char* buf; char ret[65535];
		memset(ret, 0, 65535);
		//----------
		buf = (char*)malloc(fdesc_size + 1);
		memset(buf, 0, fdesc_size + 1);
		fread(buf, 1, fdesc_size, fdesc);
		buf[fdesc_size] = 0;
		fclose(fdesc);
		//----------
		char* c1 = strstr(buf, "<name>");
		if (c1) {
			c1 += 6;
			char* c2 = strstr(c1, "</name>" );
			//----------
			if (c2) {
				memcpy(ret, c1, c2 - c1);
				free(buf);
				return std::string(ret);
			}
		}
		free(buf);
	}
	return std::string(" --- broken: ") + path + std::string(" broken ---");
}


std::string wstrtostr(const std::wstring& wstr)
{
	// Convert a Unicode string to an ASCII string
	std::string strTo;
	char* szTo = new char[wstr.length() + 1];
	szTo[wstr.size()] = '\0';
	WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), -1, szTo, (int)wstr.length(), NULL, NULL);
	strTo = szTo;
	delete[] szTo;
	return strTo;
}

bool subscribe(std::string token, long game_id, long mod_id) {
	HINTERNET hOpenHandle, hResourceHandle, hConnectHandle;

	char address[1024]; sprintf(address, /*"https://api.mod.io/"*/"v1/games/%d/mods/%d/subscribe", game_id, mod_id);

	//const char* szHeaders = "Content-Type:application/json; charset=utf-8\r\n";
	char header[65535]; sprintf(header,
		"Accept: application/json\n"
		"Authorization: Bearer %s\n"
		"Content-Type: application/x-www-form-urlencoded",
		token.c_str());


	hOpenHandle = InternetOpenA("HTTPS", INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0);
	if (hOpenHandle == NULL)
	{
		std::cerr << "hOpenHandle == NULL" << std::endl;
		return false;
	}


	hConnectHandle = InternetConnectA(hOpenHandle,
		"api.mod.io",//address,
		INTERNET_DEFAULT_HTTPS_PORT,
		NULL, NULL, INTERNET_SERVICE_HTTP,
		0, 1);

	if (hConnectHandle == NULL)
	{

		std::cerr << "hConnectHandle == NULL" << std::endl;
		InternetCloseHandle(hOpenHandle);
		return false;
	}


	hResourceHandle = HttpOpenRequestA(hConnectHandle,
		"POST",
		address,
		NULL, NULL, NULL, INTERNET_FLAG_SECURE | INTERNET_FLAG_KEEP_CONNECTION,
		1);

	if (hResourceHandle == NULL)
	{
		std::cerr << "hResourceHandle == NULL" << std::endl;
		InternetCloseHandle(hOpenHandle);
		InternetCloseHandle(hConnectHandle);
		return false;
	}

	//InternetSetOption(hResourceHandle, INTERNET_OPTION_USERNAME, (LPVOID)USERNAME, _tcslen(USERNAME));
	//InternetSetOption(hResourceHandle, INTERNET_OPTION_PASSWORD, (LPVOID)PASSWORD, _tcslen(PASSWORD));

	std::string buf;
	if (HttpSendRequestA(hResourceHandle, header, strlen(header), NULL, 0))
	{
		while (true)
		{
			std::string part;
			DWORD size;
			if (!InternetQueryDataAvailable(hResourceHandle, &size, 0, 0))break;
			if (size == 0)break;
			part.resize(size);
			if (!InternetReadFile(hResourceHandle, &part[0], part.size(), &size))break;
			if (size == 0)break;
			part.resize(size);
			buf.append(part);
		}
	}
	else {
		std::cerr << "HttpSendRequestA is false" << std::endl;
		//char lpMsgBuf[655535];
		//LPVOID lpMsgBuf;
		//LPVOID lpDisplayBuf;
		
		DWORD dw = GetLastError();
		/*
		FormatMessageA(
			// FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			dw,
			MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT),
			(LPSTR)lpMsgBuf,//(LPTSTR)&lpMsgBuf,
			0, NULL);
		*/
		_com_error error(dw);

		std::cerr << "Last-Error is " << dw << ":" << /* lpMsgBuf */  wstrtostr(error.ErrorMessage()) << std::endl;
		//LocalFree(lpMsgBuf);
		system("pause");
	}

	if (!buf.empty())
	{
		// Get data back
		if (buf.find("\"error\":") != std::string::npos) {
			std::cerr << "\n\n\n" << buf << "\n\n\n" << std::endl;
			system("pause");
		}

	}
	else {
		std::cerr << "buf is empty" << std::endl;
		system("pause");
	}

	InternetCloseHandle(hResourceHandle);
	InternetCloseHandle(hConnectHandle);
	InternetCloseHandle(hOpenHandle);
}

// #define z_path "%appdata%\\Fancy Fish Games\\Aground\\.modio\\mods\\"
#define z_path "\\Fancy Fish Games\\Aground\\.modio\\mods\\"

void main(void) {
	_WIN32_FIND_DATAA FindFileData;
	char* appdata = getenv("APPDATA");
	std::string find_path = std::string(appdata) + z_path;
	HANDLE hFind = FindFirstFileA( (find_path  + "*").c_str(), &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE) {
		printf("FindFirstFile failed (%d)\n", GetLastError());

		system("pause");
		return;
	}

	if (auto input = std::ifstream("mod_ID_list.txt")) {
		//long api_key = 0;
		std::string token;
		std::cout << "found saved modlist, enter token to subscribe (or type \"skip\") : " << std::endl;
		std::cin >> token;

		if (token.length() > 9) {
			std::cout << "subscribing ";
			while (true) {
				long mod_id;
				input >> mod_id;
				if (input.eof()) break;
				//cerr << mod_id << endl;
				subscribe(token, 34, mod_id);
				std::cout << ".";
			}
			std::cout << std::endl;
			system("pause");
			exit(0);
		}
	}

	std::ofstream f_out = std::ofstream("mod_name_list.txt");
	std::ofstream id_out = std::ofstream("mod_ID_list.txt");
	do {
		if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			// printf( "  %s   <DIR>\n", wstrtostr(FindFileData.cFileName).c_str());
			// std::string found = wstrtostr(FindFileData.cFileName);
			std::string found(FindFileData.cFileName);
			if (found != std::string(".") && found != std::string("..")) {
				std::string str_out(get_mod_name_from_xml(
					find_path + found +
					std::string("\\mod.xml")));
				std::cout << str_out << std::endl;
				f_out << str_out << std::endl;
				id_out << found << std::endl;
			}
		}
		else
		{
			// printf("  %s \n", FindFileData.cFileName);
		}
	} while (FindNextFileA(hFind, &FindFileData) != 0);

	system("pause");
}